$(document).ready(function () {
	$('#1').addClass('active');
	(function() {
	  $.getJSON( 'data/persons.json', {
	    format: "json"
	  })
	    .done(function( data ) {
	    	$.each( data, function( i, item ) {
		        $( "#persons" ).append('<tr>'+
		        	'<td>'+i.toString()+'</td>'+
		        	'<td>'+item["first_name"]+'</td>'+
		        	'<td>'+item["last_name"]+'</td>'+
		        	'<td>'+item["middle_name"]+'</td>'+
		        	'<td>'+item["email"]+'</td>'+
		        	'<td>'+item["phone_number"]+'</td>'+
		        	'</tr>');
		    ;})
	    })
	    .fail(function(){alert('fail load json');});
	})();
});

$('.btn-default').click(function  () {
	$('.btn-default').removeClass('active');
	$(this).addClass('active');
});